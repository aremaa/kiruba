 <!DOCTYPE HTML>
<html>
<head>
  <title>QUIZ</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
	<link rel="stylesheet" href="start/jquery-ui.css">
	<script src="jquery/jquery-1.10.2.js"></script>
	<script src="jquery/ui/jquery-ui.js"></script>  
  <style>
.ui-tabs-vertical { width: 65em; padding-right:1.0em }
.ui-tabs-vertical .ui-tabs-nav { padding: 1.1em 1.1em 0.2em 0.2em; margin-top:60px; margin-left:25px; float: right; width: 25em; }
.ui-tabs-vertical .ui-tabs-nav li { clear: right; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
.ui-tabs-vertical .ui-tabs-nav li a { display:block; }
.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: 0.1em; border-right-width: 21px;}
.ui-tabs-vertical .ui-tabs-panel { padding:1em; float: inherit; width: 63.5em; background-color:#F30}
</style>   
 <script>
$(function() {
$( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
$( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
});
 $(function() {
$( "#accordion" ).accordion();
});
</script>
<link href='http://fonts.googleapis.com/css?family=Fredericka+the+Great' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Akronim' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Black+Ops+One' rel='stylesheet' type='text/css'>
</head>
<body>
  <div id="main">
    <header>
      <div id="logo">
        <div id="logo_text">
          <h1><a href="index.php"><span class="logo_colour" style="font-family: 'Black Ops One', cursive;font-size:100px;">&nbsp;QuizT</span></a></h1>
        </div>
      </div>
      <nav>
        <div id="menu_container">
          <ul class="sf-menu" id="nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="result.php">Result</a></li>
            <li><a href="">Scheme</a></li>            
            <li><a href="">Help</a></li>                        
            <li><a href="">Contact Us</a></li>
      <li><a href="resultdownload.php">Download Results</a></li>            
            <li><a href=""><div id="time" class="sf-menu"></div></a></li>
            <li><a href=""><div id="time1" class="sf-menu"></div></a></li>
          </ul>
        </div>
      </nav>
  </header>
    <div id="site_content">  
    <p>
    <div id="tabs">
<ul>
<li style="font-weight:bold;height:100px;font-size:20px"><a href="#tabs-1">QuizHead</br> MONDAY 11:00:00 AM</a></li>
<li style="font-weight:bold;height:100px;font-size:20px"><a href="#tabs-2">QuizHead</br> MONDAY 01:00:00 PM</a></li>
<li style="font-weight:bold;height:100px;font-size:20px"><a href="#tabs-3">QuizHead</br> MONDAY 06:00:00 PM</a></li>
<li style="font-weight:bold;height:100px;font-size:20px"><a href="#tabs-4">QuizHead</br> MONDAY 07:25:00 PM</a></li>
</ul>
        <div id="tabs-1">
                <h2 style="color:#F00;background-color:#FFF;">&nbsp;&nbsp;QuizHead MONDAY 11:00:00 AM</h2>
        <table border="0" cellpadding="1" cellspacing="1"  width="450" height="460" style="background-color:#90F" >
        	<tr> 
                <td width="50%" style="background-color:#FFEB3B; color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold" >Comptetion No </td>
                <td width="50%"  style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bolder">168</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold"> Comptetion Date </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">01-06-2020</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                3 D Prize Rs.27000</td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">119</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                2 D Prize Rs.1000                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
               19</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                1 D Prize Rs.100                
                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">9</td>            
            </tr>
        </table>
        </div>
        <div id="tabs-2">
                <h2 style="color:#F00;background-color:#FFF;">&nbsp;&nbsp;QuizHead MONDAY 01:00:00 PM</h2>
        <table border="0" cellpadding="1" cellspacing="1"  width="450" height="460" style="background-color:#90F" >
        	<tr> 
                <td width="50%" style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold" >Comptetion No </td>
                <td width="50%"  style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bolder">168</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold"> Comptetion Date </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">01-06-2020</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                3 D Prize Rs.27000</td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">698</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                2 D Prize Rs.1000                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
              98</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                1 D Prize Rs.100                
                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">8</td>            
            </tr>
        </table>
        </div>
        <div id="tabs-3">
                <h2 style="color:#F00;background-color:#FFF;">&nbsp;&nbsp;QuizHead MONDAY 06:00:00 PM</h2>
        <table border="0" cellpadding="1" cellspacing="1"  width="450" height="460" style="background-color:#90F" >
        	<tr> 
                <td width="50%" style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold" >Comptetion No </td>
                <td width="50%"  style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bolder">168</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold"> Comptetion Date </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">01-06-2020</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                3 D Prize Rs.27000</td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">347</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                2 D Prize Rs.1000                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
               47</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                1 D Prize Rs.100                
                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">7</td>            
            </tr>
        </table>
        </div>
        <div id="tabs-4">
                <h2 style="color:#F00;background-color:#FFF;">&nbsp;&nbsp;QuizHead MONDAY 07:25:00 PM</h2>
        <table border="0" cellpadding="1" cellspacing="1"  width="450" height="460" style="background-color:#90F" >
        	<tr> 
                <td width="50%" style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold" >Comptetion No </td>
                <td width="50%"  style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bolder">168</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold"> Comptetion Date </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">01-06-2020</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                3 D Prize Rs.27000</td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">209</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                2 D Prize Rs.1000                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
              09</td>            
            </tr>
        	<tr>
                <td style="background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">
                1 D Prize Rs.100                
                </td>
                <td style="padding-left:30px;background-color:#FFEB3B;color:#030;font:Verdana, Geneva, sans-serif;font-size:18px;font-weight:bold">9</td>            
            </tr>
        </table>
        </div>
        </div>
</p>    
<!--      <div id="sidebar_container">-->
<!-- 		<div>
          <h3>Latest News</h3>
          <h4>New Website Launched</h4>
          <h5>August 15th, 2014</h5>
          <p></p>
        </div>
-->        
<!--			<p style="font-family:Verdana, Geneva, sans-serif;font-size:10px;text-align:center" >
            | <a href="index.php">Home</a> | 
            <a href="result.php">Result</a> | 
            <a href="">Scheme</a> |
            <a href="">Help</a> | 
            <a href="">Contact Us</a> | 
          </p>
-->      
      <div class="content">
      </div>
    </div>
  </div>
  <!-- javascript at the bottom for fast page loading 
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>-->
</body>
</html>
